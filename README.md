# Fractals
Программы рисующая фракталы: Дерево Пифагора, Кривая Гилберта, Треугольник Серпинского и Квази-клевер

Для каждого фрактала можно задать глубину рекурсии и размер фрагмента.
## Menu
![alt text](KDZ1/images/menu.png)
## Примеры работы
![alt text](KDZ1/images/tree.png)
![alt text](KDZ1/images/triangle.png)
![alt text](KDZ1/images/circle.png)
