﻿using NLog;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
namespace MainForm {
	[System.ComponentModel.TypeConverter(typeof(System.Windows.PointConverter))]

    class GilbertCurve : Fractal {
        public bool[] flags;
        public GilbertCurve(Color startColor, Color endColor, int maxRecursionLevel, Logger log, double length, Point point, Canvas grid) 
            : base(startColor, endColor, maxRecursionLevel, length, point, grid) {
            flags = new bool[30];
        }
        public override void Draw() {
            var step = new Square(Size, Position, 0, 0, this, 0, 0);
            step.Draw();
        }
    }
}
