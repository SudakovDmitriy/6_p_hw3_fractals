﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using static System.Math;
namespace MainForm {
    class Square : DrawableObject {
        int type;
        int Location { get; }
        GilbertCurve owner;
        int level;
        int num;
        const int width = 4;
        public int Type {
            get => type;
            set {
                if (type < 0 || type > 3)
                    throw new ArgumentException("Тип квадрата может быть не остатком по модулю 4");
                type = value;
            }
        }

        public Square(double size, Point position, int RecursionLevel, int type, GilbertCurve owner, int loc, int num) : base(size, position) {
            level = RecursionLevel;
            this.owner = owner;
            this.type = type;
            Location = loc;
            this.num = num;
        }
        public void DrawSegment(int type, Color color) {
            if (level == owner.MaxRecursionLevel - 1) {
                var line = new Line();
                var x = Position.X;
                var y = Position.Y;
                switch (type) {
                    case 0:
                        line.X1 = x + Size / 4;
                        line.Y1 = y - Size * 3 / 4;
                        line.X2 = x + Size * 3 / 4;
                        line.Y2 = y - Size * 3 / 4;
                        break;
                    case 1:
                        line.X1 = x + Size * 3 / 4;
                        line.Y1 = y - Size * 3 / 4;
                        line.X2 = x + Size * 3 / 4;
                        line.Y2 = y - Size / 4;
                        break;
                    case 2:
                        line.X1 = x + Size * 3 / 4;
                        line.Y1 = y - Size / 4;
                        line.X2 = x + Size / 4;
                        line.Y2 = y - Size / 4;
                        break;
                    case 3:
                        line.X1 = x + Size / 4;
                        line.Y1 = y - Size / 4;
                        line.X2 = x + Size / 4;
                        line.Y2 = y - Size * 3 / 4;
                        break;
                }
                line.Stroke = new SolidColorBrush(color);
                line.StrokeThickness = width;
                owner.Canv.Children.Add(line);
            }
        }
        public override void Draw() {
            if (level >= owner.MaxRecursionLevel)
                return;

            var x = Position.X;
            var y = Position.Y;
            owner.flags[level] = true;
            int curNum = num;
            if (!owner.flags[level + 1])
                curNum++;
            var s11 = new Square(Size / 2, new Point(x, y - Size / 2), level + 1, 0, owner, 11, curNum);
            var s12 = new Square(Size / 2, new Point(x + Size / 2, y - Size / 2), level + 1, 0, owner, 12, num);
            var s21 = new Square(Size / 2, new Point(x, y), level + 1, 0, owner, 21, num);
            var s22 = new Square(Size / 2, new Point(x + Size / 2, y), level + 1, 0, owner, 22, num);


            switch (type) {
                case 0:
                    s11.Type = 3;
                    s12.Type = 1;
                    s21.Type = 0;
                    s22.Type = 0;

                    break;
                case 1:
                    s11.Type = 1;
                    s12.Type = 0;
                    s21.Type = 1;
                    s22.Type = 2;

                    break;
                case 2:
                    s11.Type = 2;
                    s12.Type = 2;
                    s21.Type = 3;
                    s22.Type = 1;

                    break;
                case 3:
                    s11.Type = 0;
                    s12.Type = 3;
                    s21.Type = 2;
                    s22.Type = 3;
                    break;
            }
            var step = Size / Pow(2, owner.MaxRecursionLevel - level) / 2;
            var color = owner.GetColor(num);
            s11.Draw();
            s12.Draw();
            s21.Draw();
            s22.Draw();


            switch (type) {
                case 0:
                    DrawSegment(1, color);
                    DrawSegment(2, color);
                    DrawSegment(3, color);
                    DrawBridge(x + step, y - Size / 2 - step, x + step, y - Size / 2 + step, color);
                    DrawBridge(x + Size - step, y - Size / 2 - step, x + Size - step, y - Size / 2 + step, color);
                    DrawBridge(x + Size / 2 - step, y - Size / 2 + step, x + Size / 2 + step, y - Size / 2 + step, color);
                    break;
                case 1:
                    DrawSegment(0, color);
                    DrawSegment(2, color);
                    DrawSegment(3, color);
                    DrawBridge(x + Size / 2 - step, y - Size + step, x + Size / 2 + step, y - Size + step, color);
                    DrawBridge(x + Size / 2 - step, y - step, x + Size / 2 + step, y - step, color);
                    DrawBridge(x + Size / 2 - step, y - Size / 2 - step, x + Size / 2 - step, y - Size / 2 + step, color);
                    break;
                case 2:
                    DrawSegment(0, color);
                    DrawSegment(1, color);
                    DrawSegment(3, color);
                    DrawBridge(x + step, y - Size / 2 - step, x + step, y - Size / 2 + step, color);
                    DrawBridge(x + Size - step, y - Size / 2 - step, x + Size - step, y - Size / 2 + step, color);
                    DrawBridge(x + Size / 2 - step, y - Size / 2 - step, x + Size / 2 + step, y - Size / 2 - step, color);
                    break;
                case 3:
                    DrawSegment(0, color);
                    DrawSegment(1, color);
                    DrawSegment(2, color);
                    DrawBridge(x + Size / 2 - step, y - Size + step, x + Size / 2 + step, y - Size + step, color);
                    DrawBridge(x + Size / 2 - step, y - step, x + Size / 2 + step, y - step, color);
                    DrawBridge(x + Size / 2 + step, y - Size / 2 - step, x + Size / 2 + step, y - Size / 2 + step, color);
                    break;
            }
        }
        public void DrawBridge(double x1, double y1, double x2, double y2, Color color) {
            if (true) {
                if (level >= owner.MaxRecursionLevel - 1) return;
                var line = new Line();
                line.X1 = x1;
                line.Y1 = y1;
                line.X2 = x2;
                line.Y2 = y2;
                line.Stroke = new SolidColorBrush(color);
                line.StrokeThickness = width;
                owner.Canv.Children.Add(line);
            }
        }
    }
}
