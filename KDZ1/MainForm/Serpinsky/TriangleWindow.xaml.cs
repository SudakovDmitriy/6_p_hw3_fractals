﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using NLog;
namespace MainForm {
    /// <summary>
    /// Логика взаимодействия для TriangleWindow.xaml
    /// </summary>
    public partial class TriangleWindow : Window {
        bool drag = false;
        Logger log;
        string method;
        Point startMousePoint;
        static double fracGiridHeightCoef = 0.9,
                fracGiridWidthCoef = 0.7;
        static bool draw = false;
        const double coef = 0.95;
        TriangleSerpinky fractal;
        Dictionary<string, Color> colors = new Dictionary<string, Color>() {
            [nameof(Colors.Black)] = Colors.Black,
            [nameof(Colors.Blue)] = Colors.Blue,
            [nameof(Colors.Red)] = Colors.Red,
            [nameof(Colors.Aqua)] = Colors.Aqua,
            [nameof(Colors.Magenta)] = Colors.Magenta,
            [nameof(Colors.Green)] = Colors.Green,
            [nameof(Colors.Khaki)] = Colors.Khaki,
            [nameof(Colors.DarkKhaki)] = Colors.DarkKhaki,
            [nameof(Colors.DarkMagenta)] = Colors.DarkMagenta,
            [nameof(Colors.DarkCyan)] = Colors.DarkCyan,
            [nameof(Colors.DarkBlue)] = Colors.DarkBlue,
            [nameof(Colors.DarkRed)] = Colors.DarkRed,
            [nameof(Colors.Tan)] = Colors.Tan,
            [nameof(Colors.LightGray)] = Colors.LightGray,

        };
        Dictionary<string, Color> backgroundColors = new Dictionary<string, Color>() {
            [nameof(Colors.Black)] = Colors.Black,
            [nameof(Colors.White)] = Colors.White,
            [nameof(Colors.DarkBlue)] = Colors.DarkBlue,
            [nameof(Colors.DarkRed)] = Colors.DarkRed,
            [nameof(Colors.DarkGreen)] = Colors.DarkGreen,
            [nameof(Colors.DarkCyan)] = Colors.DarkCyan,
            [nameof(Colors.DarkMagenta)] = Colors.DarkMagenta,
            [nameof(Colors.Tan)] = Colors.Tan,
            [nameof(Colors.Khaki)] = Colors.Khaki,
            [nameof(Colors.Firebrick)] = Colors.Firebrick,
            [nameof(Colors.Violet)] = Colors.Violet,
            [nameof(Colors.Green)] = Colors.Green,
            [nameof(Colors.Blue)] = Colors.Blue,
            [nameof(Colors.Red)] = Colors.Red,
            [nameof(Colors.Transparent)] = Colors.Transparent,
        };

        public TriangleWindow(Logger log) {
            this.log = log;
            log.Info($"Создано окно {GetType()}");
            InitializeComponent();
            method = "Вызов метода " + GetType().Name + ".";
            log.Info(method + "InitializeComponent");
            fractal = new TriangleSerpinky(colors["Blue"], colors["Red"], 5,log, fracGrid.Width, new Point(0, fracGrid.Height), fracGrid);
            foreach (var item in colors) {
                AddComboBoxElem(item, startColorCheckBox);
                AddComboBoxElem(item, endColorCheckBox);
            }
            foreach (var item in backgroundColors)
                AddComboBoxElem(item, backgroundColorCheckBox);
            backgroundColorCheckBox.SelectedIndex = 7;
            startColorCheckBox.SelectedIndex = 0;
            endColorCheckBox.SelectedIndex = 1;
        }
        private void AddComboBoxElem(KeyValuePair<string, Color> item, ComboBox box) {
            var newEl = new ComboBoxItem();
            newEl.Height = 20;
            newEl.Background = new SolidColorBrush(item.Value);
            newEl.Content = item.Key;
            box.Items.Add(newEl);
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e) {
            log.Info(method + "Window_SizeChanged");
            if (double.IsNaN(Height + Width))
                return;
            double newSize = Math.Min(mainCanv.ActualHeight * fracGiridHeightCoef,
                mainCanv.ActualWidth * fracGiridWidthCoef);
            fracGrid.Height = newSize;
            fracGrid.Width = newSize;
            fractal.Size = newSize * mult * coef;
            fractal.Position = new Point((fracGrid.Width - fractal.Size) / 2,
                fracGrid.Width - (fracGrid.Height - Math.Sqrt(3) / 2 * fractal.Size) / 2);
            double offset = (mainCanv.ActualHeight - fracGrid.Height) / 2;
            Canvas.SetTop(fracGrid, offset);
            Redraw();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e) {
            log.Info(method + "Window_Loaded");
            double height = (SystemParameters.PrimaryScreenHeight + SystemParameters.CaptionHeight);
			MinWidth = SystemParameters.PrimaryScreenWidth / 2;
			MinHeight = height / 2;
			MaxHeight = height;
			MaxWidth = SystemParameters.PrimaryScreenWidth;
			fractal.MaxRecursionLevel = 6;

        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            log.Info(method + "Button_Click");
            draw = !draw;
            (sender as Button).Content = (draw) ? "Hide" : "Draw";
            Redraw();
        }

        private void Redraw() {
            log.Info(method + "Redraw");
            if (draw) {
                fracGrid.Children.Clear();
                fractal.Draw();
            }
            else {
                fracGrid.Children.Clear();
                log.Info(method + "fracGrid.Children.Clear");
            }
        }

        int mult = 1;

        private void RecLvl_PreviewTextInput(object sender, TextCompositionEventArgs e) {
            log.Info(method + "RecLvl_PreviewTextInput");
            Regex regex = new Regex("^[0-9]+");
            int res;
            if (regex.IsMatch(e.Text) && int.TryParse(RecLvl.Text + e.Text, out res) && res <= 7 && res >= 1) {
                fractal.MaxRecursionLevel = res;
                e.Handled = false;
                Redraw();
            }
            else {
                e.Handled = true;
                log.Warn("Некорректный ввод глубины рекурсии");
                MessageBox.Show("Incorrect value for recursion number.");
            }
        }
        private void fracGrid_MouseMove(object sender, MouseEventArgs e) {
            log.Info(method + "fracGrid_MouseMove");
            if (!drag || !draw || mult == 1)
                return;
            var newPoint = e.MouseDevice.GetPosition(this);
            double dx = (startMousePoint.X - newPoint.X),
                dy = (startMousePoint.Y - newPoint.Y),
                borderCoef = fractal.Size * mult * 0.6;

            double newX = Math.Max(Math.Min(borderCoef, fractal.Position.X - dx), -borderCoef),
                newY = Math.Max(Math.Min(borderCoef, fractal.Position.Y - dy), -borderCoef);
            fractal.Position = new Point(newX, newY);
            startMousePoint = newPoint;
            Redraw();
        }


        private void fracGrid_MouseDown(object sender, MouseButtonEventArgs e) {
            log.Info(method + "fracGrid_MouseDown");
            startMousePoint = e.MouseDevice.GetPosition(this);
            drag = true;
        }

        private void fracGrid_MouseUp(object sender, MouseButtonEventArgs e) {
            log.Info(method + "fracGrid_MouseUp");
            drag = false;
        }


        private void CheckBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            log.Info(method + "CheckBox_SelectionChanged");
            if (fractal is null || startColorCheckBox.SelectedIndex < 0 || endColorCheckBox.SelectedIndex < 0)
                return;
            string s1 = (startColorCheckBox.SelectedItem as ComboBoxItem).Content.ToString();
            string s2 = (endColorCheckBox.SelectedItem as ComboBoxItem).Content.ToString();
            fractal.StartColor = colors[s1];
            fractal.EndColor = colors[s2];
            Redraw();
        }
        private void BackGroundCheckBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            log.Info(method + "BackGroundCheckBox_SelectionChanged");
            string s = (backgroundColorCheckBox.SelectedItem as ComboBoxItem).Content.ToString();
            SolidColorBrush brush = new SolidColorBrush(backgroundColors[s]);
            fracGrid.Background = brush;
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e) {
            log.Info(method + "RadioButton_Checked");
            if (fractal is null)
                return;
            double mult = ((sender as RadioButton).Content.ToString()[1] - '0');
            fractal.Size = fractal.Size / this.mult * mult;
            this.mult = (int)mult;
            fractal.Position = new Point((fracGrid.Width - fractal.Size) / 2, (fracGrid.Height + Math.Sqrt(3) / 2 * fractal.Size) / 2);
            Redraw();
        }

        private void fracGrid_Loaded(object sender, RoutedEventArgs e) {
            log.Info(method + "fracGrid_Loaded");
            Window_SizeChanged(this, null);
        }
        private void Save_Button_Click(object sender, RoutedEventArgs e) {
            log.Info(method + "Save_Button_Click");
            var image = GetImage(fracGrid);
            using (FileStream fs = new FileStream(@"../../../Triangle.png", FileMode.Create)) {
                SaveAsPng(image, fs);
                log.Info("Картинка сохранена в файл \"Curve.png\"");
                MessageBox.Show("Картинка сохраненна в файл \"Triangle.png\"", "Оповещение", MessageBoxButton.OK);
            }
        }
        public static RenderTargetBitmap GetImage(Canvas view) {
            Size size = new Size(view.ActualWidth, view.ActualHeight);
            if (size.IsEmpty)
                return null;

            RenderTargetBitmap result = new RenderTargetBitmap((int)size.Width, (int)size.Height, 96, 96, PixelFormats.Pbgra32);

            DrawingVisual drawingvisual = new DrawingVisual();
            using (DrawingContext context = drawingvisual.RenderOpen()) {
                context.DrawRectangle(new VisualBrush(view), null, new Rect(new Point(), size));
                context.Close();
            }

            result.Render(drawingvisual);
            return result;
        }

        public static void SaveAsPng(RenderTargetBitmap src, Stream outputStream) {
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(src));
            encoder.Save(outputStream);
        }
    }
}
