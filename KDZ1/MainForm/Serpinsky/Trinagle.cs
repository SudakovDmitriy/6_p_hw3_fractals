﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;
using static System.Math;

namespace MainForm {
    class Trinagle : DrawableObject {
        int level;
        const int width = 4;
        Point A { get; }
        Point B { get; }
        Point C { get; }
        TriangleSerpinky owner;
        public Trinagle(double size, Point position, int RecursionLevel, TriangleSerpinky owner) : base(size, position) {
            level = RecursionLevel;
            this.owner = owner;
            A = position;
            B = new Point(position.X + size, position.Y);
            C = new Point(position.X + Size / 2, position.Y - Size * Sin(PI / 3));
        }
        public override void Draw() {
            if (level >= owner.MaxRecursionLevel)
                return;
            var x = Position.X;
            var y = Position.Y;
            Point A = Position;
            Point B = new Point(x + Size / 2, y);
            Point C = new Point(x + Size / 4, y - Size / 2 * Sin(PI / 3));
            var leftTriangle = new Trinagle(Size / 2, A, level + 1, owner);
            var rightTriangle = new Trinagle(Size / 2, B, level + 1, owner);
            var topTriangle = new Trinagle(Size / 2, C, level + 1, owner);
            leftTriangle.Draw();
            rightTriangle.Draw();
            topTriangle.Draw();
            DrawLine(this.A, this.B);
            DrawLine(this.A, this.C);
            DrawLine(this.B, this.C);
        }
        public void DrawLine(Point a, Point b) {

            var line = new Line();
            line.X1 = a.X;
            line.Y1 = a.Y;
            line.X2 = b.X;
            line.Y2 = b.Y;
            line.Stroke = new SolidColorBrush(owner.GetColor(level));
            line.StrokeThickness = width;
            owner.Canv.Children.Add(line);
        }
    }
}
