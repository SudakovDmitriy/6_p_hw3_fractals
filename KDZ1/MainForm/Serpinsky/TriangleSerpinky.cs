﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using NLog;
namespace MainForm {
    class TriangleSerpinky : Fractal {
        public TriangleSerpinky(Color startColor, Color endColor, int maxRecursionLevel, Logger log, double length, Point point, Canvas grid) :
            base(startColor, endColor, maxRecursionLevel, length, point, grid) {
        }

        public override void Draw() {
            var startTriangle = new Trinagle(Size, Position, 0, this);
            startTriangle.Draw();
        }
    }
}
