﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using static MainForm.GeneralMethods;

namespace MainForm {
	/// <summary>
	/// Логика взаимодействия для GilbertCurveWindow.xaml
	/// </summary>
	public partial class GilbertCurveWindow : Window {
		Fractal fractal;

		public GilbertCurveWindow(Logger logger) {
			GeneralMethods.Init("HilbertCruveWindow", logger);
			InitializeComponent();
			baseProportionCoef = 0.95;
			double ns = fracCanv.Height * baseProportionCoef;
			Point np = new Point((fracCanv.Width - ns) / 2,
				(fracCanv.Height + ns) / 2);
			fractal = new GilbertCurve(colors["Blue"], colors["Black"], 3, logger, ns, np, fracCanv);
			InitCheckBoxes(startColorComboBox, 0);
			InitCheckBoxes(endColorComboBox, 1);
			fractal.ProportionCoef = baseProportionCoef;
		}

		private void Window_SizeChanged(object sender, SizeChangedEventArgs e) {
			GeneralMethods.Window_SizeChanged(this, mainCanv, fracCanv);
			//fractal.Position = new Point((fracCanv.Width - fractal.Size) / 2,
			//    (fracCanv.Height + fractal.Size) / 2);
			Redraw();


		}
		private void Window_Loaded(object sender, RoutedEventArgs e) {
			GeneralMethods.Window_Loaded(this);
			//MessageBox.Show($"H : {MinHeight}, W : {MinWidth}");

		}

		public void HideDrawButton_Click(object sender, RoutedEventArgs e) {
			GeneralMethods.Button_Click(sender);
			Redraw();
		}

		private void Redraw() {
			if (draw) {
				fracCanv.Children.Clear();
				fractal.Size = fracCanv.Height * fractal.ProportionCoef;
				fractal.Draw();
			}
			else {
				fracCanv.Children.Clear();
			}
		}


		private void RecLvl_PreviewTextInput(object sender, TextCompositionEventArgs e) {
			GeneralMethods.TextBoxRecLvl_TextChanged(fractal, RecLvlTextBox, e, 6);
			Redraw();
		}
		private void fracCanv_MouseMove(object sender, MouseEventArgs e) {
			GeneralMethods.FracCanv_MouseMoved(fractal, this, e);
			Redraw();
		}


		private void fracCanv_MouseDown(object sender, MouseButtonEventArgs e) {
			GeneralMethods.FracCanv_MouseDown(this, e);
		}

		private void mainCanv_MouseUp(object sender, MouseButtonEventArgs e) {
			GeneralMethods.fracCanv_MouseUp();
		}


		private void CheckBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			GeneralMethods.CombokBox_SelectionChanged(fractal, startColorComboBox, endColorComboBox);
			Redraw();
		}
		private void RadioButton_Checked(object sender, RoutedEventArgs e) {
			GeneralMethods.RadioButton_Checked(fractal, sender);
			if (!(fractal is null)) {
				fractal.Size = fracCanv.Height * fractal.ProportionCoef;
				fractal.Position = new Point((fracCanv.Width - fractal.Size) / 2,
				(fracCanv.Height + fractal.Size) / 2);
			}
			Redraw();
		}

		private void fracGrid_Loaded(object sender, RoutedEventArgs e) {
			Window_SizeChanged(this, null);
		}

		private void Save_Button_Click(object sender, RoutedEventArgs e) {
			GeneralMethods.Save_Button_Click(fracCanv);
		}

		public Canvas GetFractalCanvas() => fracCanv;
		public TextBox GetRecLvlTextBox() => RecLvlTextBox;
		public Button GetHideDrawButton() => HideDrawButton;
		public Fractal GetFractal() => fractal;
	}
}

