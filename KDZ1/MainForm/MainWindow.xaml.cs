﻿using System.Windows;
using NLog;
namespace MainForm {
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {

		private static Logger logger = LogManager.GetCurrentClassLogger();
		/// <summary>
		/// Конструктор окна меню.
		/// </summary>
		public MainWindow() {
			logger.Info("Создано окно меню");
			InitializeComponent();
			logger.Info("Открыто окно меню");
		}

		/// <summary>
		/// Обработчик для запуска фрактала "Обдуваемое ветром дерево".
		/// </summary>
		private void Button_Pythagoras_Tree_Click(object sender, RoutedEventArgs e) {
			Hide();
			logger.Info("Нажата кнопка \"Дерево Пифагора\"");
			var window = new FracWindow(logger);
			logger.Info("Открыто окно фрактала \"Дерево Пифагора\"");
			window.ShowDialog();
			Show();
		}

		/// <summary>
		/// Обработчик для запуска фрактала "Кривая Гилберта".
		/// </summary>
		private void Button_Hilbert_Line_Click(object sender, RoutedEventArgs e) {
			Hide();
			logger.Info("Нажата кнопка \"Линия Гильберта\"");
			var window = new GilbertCurveWindow(logger);
			logger.Info("Открыто окно фрактала \"Линия Гильберта\"");
			window.ShowDialog();
			Show();
		}

		/// <summary>
		/// Обработчик для запуска фрактала "Треугольник Серпинского".
		/// </summary>
		private void Button_Sierpinski_Triangle_Click(object sender, RoutedEventArgs e) {
			Hide();
			logger.Info("Нажата кнопка \"Треугольник Серпинского\"");
			var window = new TriangleWindow(logger);
			logger.Info("Открыто окно фрактала \"Треугольник Серпинского\"");
			window.ShowDialog();
			Show();
		}

		/// <summary>
		/// Обработчик для запуска фрактала "Квази-клевер".
		/// </summary>
		private void Button_Quasi_Clover_Click(object sender, RoutedEventArgs e) {
			Hide();
			logger.Info("Нажата кнопка \"Квази клевер\"");
			var window = new CloverWindow(logger);
			logger.Info("Открыто окно фрактала \"Квази клевер\"");
			window.ShowDialog();
			Show();
		}
	}
}
