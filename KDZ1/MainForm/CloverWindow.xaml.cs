﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using static MainForm.GeneralMethods;

namespace MainForm {
	/// <summary>
	/// Логика взаимодействия для CloverWindow.xaml
	/// </summary>
	public partial class CloverWindow : Window {
		public CloverWindow(NLog.Logger logger) {
			GeneralMethods.Init("CloverWindow", logger);
			InitializeComponent();
			fractal = new FractalClover(0, 0.25, colors["Black"], colors["Blue"], 5, (int)fracCanv.Height, new Point(0, 0), fracCanv);
			InitCheckBoxes(startColorComboBox, 0);
			InitCheckBoxes(endColorComboBox, 1);
			baseProportionCoef = 0.2;
			fractal.ProportionCoef = baseProportionCoef;
		}



		Fractal fractal;

		private void Window_SizeChanged(object sender, SizeChangedEventArgs e) {
			GeneralMethods.Window_SizeChanged(this, mainCanv, fracCanv);
			// fracGrid.Height = mainGrid.Width * fracGiridWidthCoef;
			Redraw();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e) {
			GeneralMethods.Window_Loaded(this);
			//MessageBox.Show($"H : {MinHeight}, W : {MinWidth}");

		}

		public void HideDrawButton_Click(object sender, RoutedEventArgs e) {
			GeneralMethods.Button_Click(sender);
			Redraw();
		}

		private void Redraw() {
			if (draw) {
				fracCanv.Children.Clear();
				fractal.Size = (int)fracCanv.Height;
				fractal.Draw();
			}
			else {
				fracCanv.Children.Clear();
			}
		}


		private void RecLvl_PreviewTextInput(object sender, TextCompositionEventArgs e) {
			GeneralMethods.TextBoxRecLvl_TextChanged(fractal, RecLvlTextBox, e, 7);
			Redraw();
		}

		private void LengthSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
			if (fractal is null)
				return;
			(fractal as FractalClover).LenChangeCoef = LengthSlider.Value;
			Redraw();
		}

		private void RadioButton_Checked(object sender, RoutedEventArgs e) {
			GeneralMethods.RadioButton_Checked(fractal, sender);
			Redraw();
		}

		private void fracGrid_MouseMove(object sender, MouseEventArgs e) {
			GeneralMethods.FracCanv_MouseMoved(fractal, this, e);
			Redraw();
		}

		private void fracGrid_MouseDown(object sender, MouseButtonEventArgs e) {
			GeneralMethods.FracCanv_MouseDown(this, e);
		}

		private void fracGrid_MouseUp(object sender, MouseButtonEventArgs e) {
			GeneralMethods.fracCanv_MouseUp();
		}


		private void CheckBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			GeneralMethods.CombokBox_SelectionChanged(fractal, startColorComboBox, endColorComboBox);
			Redraw();
		}

		private void Type_Checked(object sender, RoutedEventArgs e) {
			if (fractal is null)
				return;
			var button = sender as RadioButton;
			int newType = 0;
			if (button == ButtonDown)
				newType = 0;
			if (button == ButtonUp)
				newType = 2;
			if (button == ButtonLeft)
				newType = 1;
			if (button == ButtonRight)
				newType = 3;
			(fractal as FractalClover).Type = newType;
			Redraw();
		}

		private void fracGrid_Loaded(object sender, RoutedEventArgs e) {
			Window_SizeChanged(this, null);
		}

		private void Save_Button_Click(object sender, RoutedEventArgs e) {
			GeneralMethods.Save_Button_Click(fracCanv);
		}

		public Canvas GetFractalCanvas() => fracCanv;
		public TextBox GetRecLvlTextBox() => RecLvlTextBox;
		public Button GetHideDrawButton() => HideDrawButton;
		public Fractal GetFractal() => fractal;
	}
}
