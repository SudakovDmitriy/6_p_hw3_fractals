﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MainForm {
	/// <summary>
	/// класс, описывающий элемент квазиклевера.
	/// </summary>
	internal class FractalCloverCircle : DrawableObject {
		/// <summary> Текущий уровень рекурсии. </summary>
		private int curRecLvl;
		/// <summary> Текущий тип клевера. </summary>
		private int type;
		/// <summary> Фрактал, элементом которого мы явсяемся. </summary>
		FractalClover owner;

		// 4 вектора в 2х мерном пространстве, показывающие куда смещён фрактал.
		private static int[] di = { 0, 1, 0, -1 };
		private static int[] dj = { -1, 0, 1, 0 };

		/// <summary>
		/// Конструктор, инициализирующий параметры объекта.
		/// </summary>
		/// <param name="type"> Тип клевера. </param>
		/// <param name="curRecLvl"> Текущий уровень рекурсии. </param>
		/// <param name="owner"> Фрактал, который мы рисуем. </param>
		/// <param name="length">  Размер объекта, который мы отрисовываем. </param>
		/// <param name="start"> Координаты объекта. </param>
		public FractalCloverCircle(int type, int curRecLvl, FractalClover owner,double length, Point start) 
			: base(length, start) {

			this.type = type;
			this.curRecLvl = curRecLvl;
			this.owner = owner ?? throw new ArgumentNullException("Линия фрактала обязательно должна быть частью фрактального дерева.");
		}

		public override void Draw() {
			if (curRecLvl == owner.MaxRecursionLevel)
				return;
			// Нарисуем текущий элипс, создадим ещё 3, запустим их. 
			CreateEllipse();
			double delt = Size * (owner.LenChangeCoef+1);
			if (Size * owner.LenChangeCoef > 0) {
				int cType = type,
					lType = (type + 1) % 4,
					rType = (type + 3) % 4;
				var centralChild = new FractalCloverCircle(cType, curRecLvl + 1,owner, delt-Size,
										new Point(Position.X+di[cType]*delt, Position.Y+dj[cType]*delt));
				var leftChild = new FractalCloverCircle(lType, curRecLvl + 1, owner, delt - Size,
										new Point(Position.X + di[lType] * delt, Position.Y + dj[lType] * delt));
				var rightChild = new FractalCloverCircle(rType, curRecLvl + 1, owner, delt - Size,
										new Point(Position.X + di[rType] * delt, Position.Y + dj[rType] * delt));

				centralChild.Draw();
				leftChild.Draw();
				rightChild.Draw();
			}
		}

		/// <summary>
		/// Метод создаёт, заполняет и заливает элипс, и добавляет его на форму.
		/// </summary>
		private void CreateEllipse() {
			var ellipse = new Ellipse();
			ellipse.Height = Size*2;
			ellipse.Width = Size*2;
			
			ellipse.Stroke = new SolidColorBrush(owner.GetColor(curRecLvl));
			ellipse.Fill = new SolidColorBrush(owner.GetColor(curRecLvl));
			owner.Canv.Children.Add(ellipse);
			Canvas.SetTop(ellipse, Position.Y-Size);
			Canvas.SetLeft(ellipse, Position.X-Size);
		}
	}
}