﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MainForm {
	/// <summary>
	/// Фрактал Квазиклевер.
	/// </summary>
	class FractalClover : Fractal {
		/// <summary> Коэффициент, показывающий во сколько раз следующий уровень меньше предыдущего. </summary>
		double lenChangeCoef;
		/// <summary> Тип клевера, то есть в какую сторону он повёрнут. </summary>
		private int type;

		// 4 вектора в 2х мерном пространстве, показывающие куда смещён фрактал.
		private static int[] di = { 0, 1, 0, -1 };
		private static int[] dj = { -1, 0, 1, 0 };

		/// <summary>
		/// Свойство для обращения к lenChangeCoef. 
		/// </summary>
		public double LenChangeCoef { get => lenChangeCoef; set { lenChangeCoef = value; } }
		/// <summary>
		/// Свойство для обращения к типу фрактала.
		/// </summary>
		public int Type { get => type; set => type = value; }

		/// <summary>
		/// Конструктор, инициализирующий все параметры фрактала.
		/// </summary>
		/// <param name="type"> тип фрактала, то есть куда он повёрнут. </param>
		/// <param name="lenChangeCoef"> Параметр изменения длины. </param>
		/// <param name="startColor"> Начальный цвет фрактала. </param>
		/// <param name="endColor"> Конечный цвет фрактала. </param>
		/// <param name="maxRecursionLevel"> Максимальный уровень рекурсии. </param>
		/// <param name="length"> Размер 1ого элемента фрактала. </param>
		/// <param name="point"> Координаты фрактала. </param>
		/// <param name="canvas"> Канвас, на котором будет нарисован фрактал. </param>
		public FractalClover(int type, double lenChangeCoef, Color startColor, Color endColor, 
			int maxRecLvl, int length, Point start, Canvas grid)
			: base(startColor, endColor, maxRecLvl, length, start, grid) {

			this.Type = type;
			this.lenChangeCoef = lenChangeCoef;
		}

		/// <summary>
		/// Метод рисования фрактала.
		/// </summary>
		public override void Draw() {
			Canv.Children.Clear();

			// Создадим родительский элемент и рекурсивано запустим от него процесс рисования.
			double parSize = Size * ProportionCoef;
			var parent = new FractalCloverCircle(Type, 0, this, parSize,
							new Point(x: Position.X + Size / 2 -di[Type]*parSize, y: Position.Y + Size/2 - dj[Type] * parSize));
			parent.Draw();
		}
	}
}
