﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;

namespace MainForm {
	/// <summary>
	/// Класс описывающий элемент фрактального дерева. 
	/// </summary>
	class FractalTreeLine : DrawableObject {
		/// <summary> Угол наклона прямой. </summary>
		double myAngle;
		/// <summary> Текущий уровень рекурсии. </summary>
		private int curRecLvl;
		/// <summary> Фрактал, элементом которого мы являемся. </summary>
		FractalTree owner;

		/// <summary>
		/// Конструктор, инициализирующий основные поля.
		/// </summary>
		/// <param name="angleMy"> Угол наклона прямой. </param>
		/// <param name="curRecLvl"> Текущий уровень рекурсии. </param>
		/// <param name="owner"> Фрактал, который мы рисуем. </param>
		/// <param name="length">  Размер объекта, который мы отрисовываем. </param>
		/// <param name="start"> Координаты объекта. </param>
		public FractalTreeLine(
			double angleMy, int curRecLvl, FractalTree owner,
			double length, Point start) : base(length, start) {

			this.myAngle = angleMy;
			this.curRecLvl = curRecLvl;
			this.owner = owner ?? throw new ArgumentNullException("Линия фрактала обязательно должна быть частью фрактального дерева.");
		}

		/// <summary>
		/// Метод отрисовки фрактального дерева.
		/// </summary>
		public override void Draw() {
			if (curRecLvl == owner.MaxRecursionLevel)
				return;
			Line line = CreateLine();
			owner.Canv.Children.Add(line);
			if (Size * owner.LenChangeCoef > 0) {
				var leftChild = new FractalTreeLine(
										myAngle - owner.AngleLeftChild, curRecLvl + 1,
										owner, (Size * owner.LenChangeCoef),
										new Point(x: line.X2, y: line.Y2));
				var rightChild = new FractalTreeLine(
										myAngle + owner.AngleRightChild, curRecLvl + 1,
										owner, (Size * owner.LenChangeCoef),
										new Point(x: line.X2, y: line.Y2));
				leftChild.Draw();
				rightChild.Draw();
			}
		}


		// Метод создани линии.
		private Line CreateLine() {
			var line = new Line();
			var p1 = Position;
			var p2 = new Point(x: (int)(Position.X + Size * Math.Sin(myAngle)),
								y: (int)(Position.Y + Size * Math.Cos(myAngle)));

			line.X1 = p1.X;
			line.Y1 = p1.Y;
			line.X2 = p2.X;
			line.Y2 = p2.Y;
			line.Stroke = new SolidColorBrush(owner.GetColor(curRecLvl));
			line.StrokeThickness = 10;
			return line;
		}
	}
}