﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using static MainForm.GeneralMethods;

namespace MainForm {
	/// <summary>
	/// Логика взаимодействия для TriangleWindow.xaml
	/// </summary>
	public partial class TriangleWindow : Window {
		Fractal fractal;

		public TriangleWindow(NLog.Logger logger) {
			GeneralMethods.Init("TriangleWindow", logger);
			baseProportionCoef = 0.95;
			InitializeComponent();
			double ns = fracCanv.Width * baseProportionCoef;
			fractal = new TriangleSerpinky(colors["Blue"], colors["Red"], 5, logger, ns,
				new Point((fracCanv.Width - ns) / 2, fracCanv.Width - (fracCanv.Height - Math.Sqrt(3) / 2 * ns) / 2),
			fracCanv);
			fractal.ProportionCoef = baseProportionCoef;

			InitCheckBoxes(startColorComboBox, 0);
			InitCheckBoxes(endColorComboBox, 1);
		}

		private void Window_SizeChanged(object sender, SizeChangedEventArgs e) {
			GeneralMethods.Window_SizeChanged(this, mainCanv, fracCanv);

			Redraw();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e) {
			GeneralMethods.Window_Loaded(this);
			fractal.MaxRecursionLevel = 5;
			//MessageBox.Show($"H : {MinHeight}, W : {MinWidth}");

		}

		public void HideDrawButton_Click(object sender, RoutedEventArgs e) {
			GeneralMethods.Button_Click(sender);
			Redraw();
		}

		private void Redraw() {
			if (draw) {
				fracCanv.Children.Clear();

				fractal.Size = fracCanv.Height * fractal.ProportionCoef;
				//fractal.Position = new Point((fracCanv.Width - fractal.Size) / 2,
				//	fracCanv.Width - (fracCanv.Height - Math.Sqrt(3) / 2 * fractal.Size) / 2);

				fractal.Draw();
			}
			else {
				fracCanv.Children.Clear();
			}
		}


		private void RecLvl_PreviewTextInput(object sender, TextCompositionEventArgs e) {
			GeneralMethods.TextBoxRecLvl_TextChanged(fractal, RecLvlTextBox, e, 6);
			Redraw();
		}
		private void fracGrid_MouseMove(object sender, MouseEventArgs e) {
			GeneralMethods.FracCanv_MouseMoved(fractal, this, e);
			Redraw();
		}


		private void fracGrid_MouseDown(object sender, MouseButtonEventArgs e) {
			GeneralMethods.FracCanv_MouseDown(this, e);
		}

		private void fracGrid_MouseUp(object sender, MouseButtonEventArgs e) {
			GeneralMethods.fracCanv_MouseUp();
		}


		private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			GeneralMethods.CombokBox_SelectionChanged(fractal, startColorComboBox, endColorComboBox);
			Redraw();
		}
		private void RadioButton_Checked(object sender, RoutedEventArgs e) {
			GeneralMethods.RadioButton_Checked(fractal, sender);
			if (!(fractal is null)) {
				fractal.Size = fracCanv.Height * fractal.ProportionCoef;
				fractal.Position = new Point((fracCanv.Width - fractal.Size) / 2,
					fracCanv.Width - (fracCanv.Height - Math.Sqrt(3) / 2 * fractal.Size) / 2);
			}
			Redraw();
		}

		private void fracGrid_Loaded(object sender, RoutedEventArgs e) {
			Window_SizeChanged(this, null);
		}

		private void Save_Button_Click(object sender, RoutedEventArgs e) {
			GeneralMethods.Save_Button_Click(fracCanv);
		}
		public Canvas GetFractalCanvas() => fracCanv;
		public TextBox GetRecLvlTextBox() => RecLvlTextBox;
		public Button GetHideDrawButton() => HideDrawButton;
		public Fractal GetFractal() => fractal;

	}
}
