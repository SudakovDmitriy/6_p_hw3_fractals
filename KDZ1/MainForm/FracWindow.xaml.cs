﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using static MainForm.GeneralMethods;

namespace MainForm {
	/// <summary>
	/// Логика взаимодействия для Window1.xaml
	/// </summary>
	public partial class FracWindow : Window {
		public FracWindow(NLog.Logger logger) {
			GeneralMethods.Init("TreeWindow", logger);
			InitializeComponent();
			fractal = new FractalTree(Math.PI / 180 * 45, Math.PI / 180 * 45, 0.65, colors["Black"], colors["Blue"], 5, fracCanv.Height, new Point(0, 0), fracCanv);
			InitCheckBoxes(startColorComboBox, 0);
			InitCheckBoxes(endColorComboBox, 1);
			baseProportionCoef = 0.3;
		}


		Fractal fractal;

		private void Window_SizeChanged(object sender, SizeChangedEventArgs e) {
			GeneralMethods.Window_SizeChanged(this, mainCanv, fracCanv);
			// fracGrid.Height = mainGrid.Width * fracGiridWidthCoef;
			Redraw();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e) {

			GeneralMethods.Window_Loaded(this);
			//MessageBox.Show($"H : {MinHeight}, W : {MinWidth}");

		}

		public void HideDrawButton_Click(object sender, RoutedEventArgs e) {
			GeneralMethods.Button_Click(sender);
			Redraw();
		}

		private void Redraw() {
			if (draw) {
				fracCanv.Children.Clear();
				fractal.Size = fracCanv.Height;
				fractal.Draw();
			}
			else {
				fracCanv.Children.Clear();
			}
		}

		private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
			if (fractal is null)
				return;
			(fractal as FractalTree).AngleLeftChild = Math.PI / 180 * LeftChildSlider.Value;
			(fractal as FractalTree).AngleRightChild = Math.PI / 180 * (90 - RightChildSlider.Value);
			Redraw();
		}


		private void RecLvl_PreviewTextInput(object sender, TextCompositionEventArgs e) {
			GeneralMethods.TextBoxRecLvl_TextChanged(fractal, RecLvlTextBox, e, 10);

			Redraw();
		}

		private void LengthSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
			if (fractal is null)
				return;
			(fractal as FractalTree).LenChangeCoef = LengthSlider.Value;

			Redraw();
		}

		private void RadioButton_Checked(object sender, RoutedEventArgs e) {
			GeneralMethods.RadioButton_Checked(fractal, sender);

			Redraw();
		}

		private void fracCanv_MouseMove(object sender, MouseEventArgs e) {
			GeneralMethods.FracCanv_MouseMoved(fractal, this, e);

			Redraw();
		}

		private void fracCanv_MouseDown(object sender, MouseButtonEventArgs e) {
			GeneralMethods.FracCanv_MouseDown(this, e);
		}

		private void mainCnav_MouseUp(object sender, MouseButtonEventArgs e) {
			GeneralMethods.fracCanv_MouseUp();
		}


		private void CheckBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			GeneralMethods.CombokBox_SelectionChanged(fractal, startColorComboBox, endColorComboBox);
			Redraw();
		}


		private void fracGrid_Loaded(object sender, RoutedEventArgs e) {
			Window_SizeChanged(this, null);
		}

		private void Save_Button_Click(object sender, RoutedEventArgs e) {
			GeneralMethods.Save_Button_Click(fracCanv);
		}

		public Canvas GetFractalCanvas() => fracCanv;
		public TextBox GetRecLvlTextBox() => RecLvlTextBox;
		public Button GetHideDrawButton() => HideDrawButton;
		public Fractal GetFractal() => fractal;

	}
}
