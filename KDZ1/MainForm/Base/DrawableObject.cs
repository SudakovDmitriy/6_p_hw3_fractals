﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;

namespace MainForm {
	/// <summary>
	/// Класс, описывающий общие характеристики и поведения для отображаемых объектов.
	/// </summary>
	public abstract class DrawableObject {
		/// <summary>
		/// Абстрактный метод будет переопределён в классах элементов фракталов.
		/// </summary>
		public  abstract void Draw();

		/// <summary> Положение объекта на плоскости. </summary>
		Point position;
		/// <summary> Размер объекта. </summary>
		private double size;

		/// <summary>
		/// Конструктор инициализирующий все поля.
		/// </summary>
		public DrawableObject(double size, Point position) {
			Size = size;
			Position = position;
		}

		/// <summary>
		/// Свойство размера с проверкой на отрицательный размер.
		/// </summary>
		public double Size {
			get { return size; }
			set {
				if (value <= 0)
					throw new ArgumentOutOfRangeException("Длина объекта должна быть больше 0.");
				size = value; 
			}
		}

		/// <summary>
		/// Свойство для обращения к положению объекта.
		/// </summary>
		public Point Position { get => position; set => position = value; }
	}
}
