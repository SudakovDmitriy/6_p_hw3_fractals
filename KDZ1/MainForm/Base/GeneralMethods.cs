﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using NLog;
namespace MainForm {
    internal static class GeneralMethods {
        /// <summary> Имя окна, которое сейчас обрабатывается. </summary>
        internal static string curWindowName = "";
        /// <summary> Информация для логера. </summary>
        internal static string infomethod = "Вызов метода ";
        /// <summary> Действующий логер. </summary
        internal static Logger log;
        /// <summary> Флаг, показывающий отображается сейчас фракстал или нет. </summary>
        internal static bool draw = false;
        /// <summary> Коэффициент показывающий отношение длины 1ого уровня фрактала к размеру поля на котором он нарисован. </summary>
        internal static double baseProportionCoef = 0.3;
        /// <summary> Коэффициенты, отвечающие за пропорции окна фрактала относительно основного окна. </summary>
        internal static double fracGiridHeightCoef = 0.95,
            fracGiridWidthCoef = 0.7;
        /// <summary> Множитель на который сейчас домножается размер фрактала. </summary>
        internal static int mult = 1;
        /// <summary> Флаг, говорящий активен ли сейчас скролинг. </summary
        internal static bool drag = false;
        /// <summary> Предыдущая точка при скролинга. </summary>
        internal static Point startMousePoint;

        /// <summary> Словарь с цветами для инициализации комбобоксов. </summary
        internal static Dictionary<string, Color> colors = new Dictionary<string, Color>() {
            [nameof(Colors.Black)] = Colors.Black,
            [nameof(Colors.Blue)] = Colors.Blue,
            [nameof(Colors.Red)] = Colors.Red,
            [nameof(Colors.Aqua)] = Colors.Aqua,
            [nameof(Colors.Magenta)] = Colors.Magenta,
            [nameof(Colors.Green)] = Colors.Green,
            [nameof(Colors.Khaki)] = Colors.Khaki,
            [nameof(Colors.DarkKhaki)] = Colors.DarkKhaki,
            [nameof(Colors.DarkMagenta)] = Colors.DarkMagenta,
            [nameof(Colors.DarkCyan)] = Colors.DarkCyan,
            [nameof(Colors.DarkBlue)] = Colors.DarkBlue,
            [nameof(Colors.DarkRed)] = Colors.DarkRed,
        };

        /// <summary>
        /// Метод инициализирует основные переменные и флаги статического класса.
        /// </summary>
        /// <param name="windowName"> Имя на данный момент рабочего окна. </param>
        /// <param name="logger"> Логгер, производящий записи. </param>
        internal static void Init(string windowName, Logger logger) {
            curWindowName = windowName;
            log = logger;
            log.Info(infomethod + GeneralMethods.curWindowName + "Init");
            draw = false;
            mult = 1;
            drag = false;
        }

        /// <summary>
        /// Метод обработки нажатия на кнопу "нарисовать".
        /// </summary>
        /// <param name="sender"> Кнопка. </param>
        internal static void Button_Click(object sender) {
            log.Info(infomethod + curWindowName + "Button_Click");

            // Поменяем флаг и сменим надпись. 
            draw = !draw;
            (sender as Button).Content = (draw) ? "Hide" : "Draw";
        }

        /// <summary>
        /// Метод добавляет элемент в комбобокс.
        /// </summary>
        /// <param name="item"> Пара ключ-значение. Ключ - название цвета, значение - сам цвет в формате Color. </param>
        /// <param name="box"> комбобокс в который добавляют элемент. </param>
        static internal void AddComboBoxElem(KeyValuePair<string, Color> item, ComboBox box) {
            var newEl = new ComboBoxItem();
            newEl.Height = 20;
            newEl.Background = new SolidColorBrush(item.Value);
            newEl.Content = item.Key;
            box.Items.Add(newEl);
        }

        /// <summary>
        /// Метод инициализирует комбобокс всеми цветами из словаря.
        /// </summary>
        /// <param name="box"> Комбобокс, который надо проинициализировать. </param>
        /// <param name="num"> Номер элемента, который будет выбран изначально. </param>
        static internal void InitCheckBoxes(ComboBox box, int num) {
            log.Info(infomethod + curWindowName + "InitCheckBoxes");

            foreach (var item in colors) {
                AddComboBoxElem(item, box);
            }

            box.SelectedIndex = num;
        }

        /// <summary>
        /// Метод, описывающий логику для изменения размеров окна.
        /// </summary>
        /// <param name="window"> Окно, которое изменило размер. </param>
        /// <param name="mainCanv"> Имя моё. </param>
        /// <param name="fracCanv"> Канвас, на котором рисуется фрактал. </param>
        static internal void Window_SizeChanged(Window window, Canvas mainCanv, Canvas fracCanv) {
            log.Info(infomethod + curWindowName + "Window_SizeChanged");

            if (double.IsNaN(window.Height + window.Width))
                return;
            // Посчитаем новый размер и переприсвоим его.
            double newSize = Math.Min(mainCanv.ActualHeight * fracGiridHeightCoef,
                mainCanv.ActualWidth * fracGiridWidthCoef);
            fracCanv.Height = newSize;
            fracCanv.Width = newSize;

            // Сдвинем сверзу.
            double offset = (mainCanv.ActualHeight - fracCanv.Height) / 2;
            Canvas.SetTop(fracCanv, offset);
        }

        /// <summary>
        /// Метод устанавливает максимальный и минимальный размер окана.
        /// </summary>
        /// <param name="window"> Окно, ращмеры которого будут определены. </param>
        static internal void Window_Loaded(Window window) {
            log.Info(infomethod + curWindowName + "Window_Loaded");

            double height = (SystemParameters.PrimaryScreenHeight + SystemParameters.CaptionHeight);
            window.MinWidth = SystemParameters.PrimaryScreenWidth / 2;
            window.MinHeight = height / 2;
            window.MaxHeight = height;
            window.MaxWidth = SystemParameters.PrimaryScreenWidth;
        }

        /// <summary>
        /// Метод, проверяющий корректность входных данных для фрактала.
        /// </summary>
        /// <param name="fractal"> Фрактал, глубина рекурсси которого будет определена. </param>
        /// <param name="textBox"> Такст бокс из которого будет взят номер. </param>
        /// <param name="e"> Параметры события. </param>
        /// <param name="max"> Максимальная глубина рекурсии). </param>
        static internal void TextBoxRecLvl_TextChanged(Fractal fractal, TextBox textBox, TextCompositionEventArgs e, int max) {
            log.Info(infomethod + curWindowName + "TextBoxRecLvl_TextChanged");

            Regex regex = new Regex("^[0-9]+");
            int res;

            if (regex.IsMatch(e.Text) && int.TryParse(textBox.Text + e.Text, out res) && res <= max && res >= 1) {
                fractal.MaxRecursionLevel = res;
                e.Handled = false;
            }
            else {
                e.Handled = true;
                MessageBox.Show("Incorrect value for recursion number.");

                log.Warn("Введенны некорректные данные в поле выбора глубины рекурсии");
            }
        }

        /// <summary>
        /// Метод, фиксирующий то, что кнопку миши отпустили и скролинг закончен.
        /// </summary>
        internal static void fracCanv_MouseUp() {
            log.Info(infomethod + curWindowName + "fracCanv_MouseUp");

            drag = false;
        }

        /// <summary>
        /// Метод описывающий как работает изменение зума.
        /// </summary>
        /// <param name="fractal"> Фрактал, размер которого будет изменён. </param>
        /// <param name="sender"> Ссылка на Radiobutton, который был выбран. </param>
        static internal void RadioButton_Checked(Fractal fractal, object sender) {
            log.Info(infomethod + curWindowName + "RadioButton_Checked");

            if (fractal is null) {
                log.Warn("Попытка обращения к пустому фракталу");

                return;
            }
            double mult = ((sender as RadioButton).Content.ToString()[1] - '0');
            GeneralMethods.mult = (int)mult;

            fractal.Position = new Point(0, +fractal.Size * 0.25 * (mult - 1));
            fractal.ProportionCoef = baseProportionCoef * mult;

        }

        /// <summary>
        /// Метод, фиксирующий передвижение мыши.
        /// </summary>
        /// <param name="fractal"> Фрактал, координаты которого будут изменены. </param>
        /// <param name="window"> Окно, по которому передвигается курсор мыши. </param>
        /// <param name="e"> Данные о передвижении мыши. </param>
        static internal void FracCanv_MouseMoved(Fractal fractal, Window window, MouseEventArgs e) {
            log.Info(infomethod + curWindowName + "FracCanv_MouseMoved");

            // Если фрактал не отображается, или не скролится, или не приближен, то игнорируем данный метод.
            if (!drag || !draw || mult == 1)
                return;

            // Вычислим смещение.
            var newPoint = e.MouseDevice.GetPosition(window);
            double dx = (startMousePoint.X - newPoint.X),
                dy = (startMousePoint.Y - newPoint.Y),
                borderCoef = fractal.Size * mult * 0.6;

            // Запишем новые координаты так, чтобы не уходить далеко от центра изображениея.
            double newX = Math.Max(Math.Min(borderCoef, fractal.Position.X - dx), -borderCoef),
                newY = Math.Max(Math.Min(borderCoef, fractal.Position.Y - dy), -borderCoef);
            // Переприсвоим позицию фрактала.
            fractal.Position = new Point(newX, newY);
            // Обновим предыдущую точку.
            startMousePoint = newPoint;

        }

        /// <summary>
        /// Метод фиксирует начало скролинга.
        /// </summary>
        /// <param name="window"> Окно в котором происходит скролинг. </param>
        /// <param name="e"> Данные о положении мыши. </param>
        internal static void FracCanv_MouseDown(Window window, MouseButtonEventArgs e) {
            log.Info(infomethod + curWindowName + "FracCanv_MouseDown");

            startMousePoint = e.MouseDevice.GetPosition(window);
            drag = true;
        }

        /// <summary>
        /// Метод сохраняет изображение в определённый файловый поток.
        /// </summary>
        /// <param name="src">  Изображение. </param>
        /// <param name="outputStream"> файловый поток, куда изображение будет сохранено. </param>
        internal static void SaveAsPng(RenderTargetBitmap src, Stream outputStream) {
            log.Info(infomethod + curWindowName + "SaveAsPng");

            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(src));
            encoder.Save(outputStream);
        }

        /// <summary>
        /// Метод обработки соьытия нажатия на клавишу "Сохранить".
        /// </summary>
        /// <param name="fracCanv"> Контрол, содержащий фрактал. </param>
        internal static void Save_Button_Click(Canvas fracCanv) {
            log.Info(infomethod + curWindowName + "SaveAsPng");

            var image = GetImage(fracCanv);
            using (FileStream fs = new FileStream($@"../../../{curWindowName}.png", FileMode.Create)) {
                SaveAsPng(image, fs);
                MessageBox.Show($"Картинка сохраненна в файл \"{curWindowName}.png\"", "Оповещение", MessageBoxButton.OK);
            }
        }

        /// <summary>
        /// Метод, получающий из канваса изображение.
        /// </summary>
        internal static RenderTargetBitmap GetImage(Canvas view) {
            log.Info(infomethod + curWindowName + "GetImage");

            Size size = new Size(view.ActualWidth, view.ActualHeight);
            if (size.IsEmpty) {
                log.Warn("Попытка получения размера пустого элемента");

                return null;
            }

            RenderTargetBitmap result = new RenderTargetBitmap((int)size.Width, (int)size.Height, 96, 96, PixelFormats.Pbgra32);

            DrawingVisual drawingvisual = new DrawingVisual();
            using (DrawingContext context = drawingvisual.RenderOpen()) {
                context.DrawRectangle(new VisualBrush(view), null, new Rect(new Point(), size));
                context.Close();
            }

            result.Render(drawingvisual);
            return result;
        }

        /// <summary>
        /// Метод обрабатывающий смену цвета фрактала.
        /// </summary>
        /// <param name="fractal"> Фрактал, цвет которого меняют. </param>
        /// <param name="startBox"> Комбобокс с первым цветом. </param>
        /// <param name="endBox"> Комбобокс с последним цветом. </param>
        internal static void CombokBox_SelectionChanged(Fractal fractal, ComboBox startBox, ComboBox endBox) {
            log.Info(infomethod + curWindowName + "CombokBox_SelectionChanged");

            if (fractal is null || startBox.SelectedIndex < 0 || endBox.SelectedIndex < 0) {
                log.Warn("Попытка обращения к пустому фракталу или поптыка выбора несуществующего цвета");
                return;
            }
            string s1 = (startBox.SelectedItem as ComboBoxItem).Content.ToString();
            string s2 = (endBox.SelectedItem as ComboBoxItem).Content.ToString();
            fractal.StartColor = colors[s1];
            fractal.EndColor = colors[s2];
        }

    }
}
