﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using NLog;

namespace MainForm {
    /// <summary>
    /// Базовый класс фрактала, содержащий основные поля. 
    /// </summary>
    public abstract class Fractal : DrawableObject {
        /// <summary> Начальный цвет. </summary>
        private Color startColor;
        /// <summary> Конечный цвет. </summary>
        private Color endColor;
        /// <summary> Глубина рекурсии. </summary>
        private int maxRecursionLevel;
        /// <summary> Канвас, на котором будет рисоваться фрактал. </summary>
        private Canvas canvas;

        /// <summary> Коэффициент отношения размера канваса к размеру 1ого элемента фрактала. </summary>
        double proportionCoef = 0.3;

        /// <summary>
        /// Конструктор инициализирующший основные поля.
        /// </summary>
        /// <param name="startColor"> Начальный цвет фрактала.</param>
        /// <param name="endColor"> Конечный цвет фрактала. </param>
        /// <param name="maxRecursionLevel"> Максимальный уровень рекурсии. </param>
        /// <param name="length"> Размер 1ого элемента фрактала. </param>
        /// <param name="point"> Координаты фрактала. </param>
        /// <param name="canvas"> Канвас, на котором будет нарисован фрактал. </param>
        protected Fractal(Color startColor, Color endColor, int maxRecursionLevel, 
            /*Logger log,*/ double length, Point point, Canvas canvas) : base(length, point) {
            this.startColor = startColor;
            this.endColor = endColor;
            this.maxRecursionLevel = maxRecursionLevel;
            //Log = log;
            this.canvas = canvas;
        }

        /// <summary> Свойство Логгер при помощи которого выводится вспомогательная информация. </summary>
        public Logger Log { get; }
        /// <summary> Свойство для максимального уровня рекурсии. </summary>
        public int MaxRecursionLevel { get => maxRecursionLevel; set { maxRecursionLevel = value; } }

        /// <summary> Свойство для начального цвета. </summary>
        public Color StartColor {get =>startColor; set { startColor = value; } }
        /// <summary> Свойство для конечного цвета. </summary>
        public Color EndColor { get => endColor; set { endColor = value; } }
        /// <summary> Свойсто для обращения к канвасу. </summary>
        public Canvas Canv { get => canvas; }
        /// <summary> Свойство для обращения к коэффициенту пропорциональности. </summary
        public double ProportionCoef { get => proportionCoef; set => proportionCoef = value; }

        /// <summary>
        /// Абстрактный метод рисования будет переопределён в классах фракталах.
        /// </summary>
        public abstract override void Draw();

        /// <summary>
        /// Метод возращает цвет по шагу рекурсии.
        /// </summary>
        /// <param name="curRecLvl"> Текущая глубина рекурсии. </param>
        /// <returns> Возвращает цвет типа Color. </returns>
        public Color GetColor(int curRecLvl) {
            return GetGradients(StartColor, EndColor, curRecLvl, MaxRecursionLevel);
        }

        /// <summary>
        /// Метод получения градиента.
        /// </summary>
        protected static Color GetGradients(Color start, Color end, int curStep, int steps) {
            byte stepA = (byte)((end.A - start.A) / (steps));
            byte stepR = (byte)((end.R - start.R) / (steps));
            byte stepG = (byte)((end.G - start.G) / (steps));
            byte stepB = (byte)((end.B - start.B) / (steps));

            return Color.FromArgb((byte)(start.A + (stepA * curStep)),
                                  (byte)(start.R + (stepR * curStep)),
                                  (byte)(start.G + (stepG * curStep)),
                                  (byte)(start.B + (stepB * curStep)));
        }
    }
}
