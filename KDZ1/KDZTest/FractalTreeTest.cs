﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using MainForm;
using NLog;

namespace KDZTest {
    [TestClass]
    public class FractalTreeTest {
		[TestMethod]
		public void HideDrawButton_Click_DrawFractal() {
			var window = new FracWindow(LogManager.GetCurrentClassLogger());
			var canvas = window.GetFractalCanvas();
			var fractal = window.GetFractal();
			var button = window.GetHideDrawButton();
			int recursionLvl = 6,
				expectedAmtOfChilds = ((int)Math.Pow(2, recursionLvl) - 1),
				actualAmtOfChilds;

			fractal.MaxRecursionLevel = recursionLvl;
			window.HideDrawButton_Click(button, new System.Windows.RoutedEventArgs());
			actualAmtOfChilds = canvas.Children.Count;

			Assert.AreEqual(expectedAmtOfChilds, actualAmtOfChilds);
		}
	}
}
