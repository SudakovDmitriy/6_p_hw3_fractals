﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MainForm;
using NLog;

namespace KDZTest {
	[TestClass]
	public class CloverWindowTest {
		[TestMethod]
		public void HideDrawButton_Click_DrawFractal() { 
			var window = new CloverWindow(LogManager.GetCurrentClassLogger());
			var canvas = window.GetFractalCanvas();
			var button = window.GetHideDrawButton();
			var fractal = window.GetFractal();
			int recursionLvl = 5,
				expectedAmtOfChilds = ((int)Math.Pow(3, recursionLvl)-1)/2,
				actualAmtOfChilds;

			fractal.MaxRecursionLevel = recursionLvl;
			window.HideDrawButton_Click(button, new System.Windows.RoutedEventArgs());
			actualAmtOfChilds = canvas.Children.Count;

			Assert.AreEqual(expectedAmtOfChilds, actualAmtOfChilds);
		}
	}
}
