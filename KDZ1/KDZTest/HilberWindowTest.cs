﻿using MainForm;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using System;

namespace KDZTest {
	[TestClass]
	public class HilberWindowTest {
		[TestMethod]
		public void HideDrawButton_Click_DrawFractal() {
			var window = new GilbertCurveWindow(LogManager.GetCurrentClassLogger());
			var canvas = window.GetFractalCanvas();
			var button = window.GetHideDrawButton();
			var fractal = window.GetFractal();
			int recursionLvl = 5,
				expectedAmtOfChilds = ((int)Math.Pow(4, recursionLvl) - 1),
				actualAmtOfChilds;

			fractal.MaxRecursionLevel = recursionLvl;
			window.HideDrawButton_Click(button, new System.Windows.RoutedEventArgs());
			actualAmtOfChilds = canvas.Children.Count;

			Assert.AreEqual(expectedAmtOfChilds, actualAmtOfChilds);
		}
	}
}
